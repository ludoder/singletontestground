﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {

    // Reference Cache
    public GameObject gameManager;
    public GameObject soundManager;

    private void Awake()
    {
        Debug.Log("LOADER -- Awake() called");

        if (GameManager.instance == null)
        {
            Debug.Log("LOADER -- GameManager.instance is NULL. Instantiating prefab...");
            Instantiate(gameManager);
        } else
        {
            Debug.Log("LOADER -- GameManager.instance is NOT NULL. Existing instance will be used. ");
        }

        if (SoundManager.instance == null)
        {
            Debug.Log("LOADER -- SoundManager.instance is NULL. Instantiating prefab...");
            Instantiate(soundManager);
        }
        else
        {
            Debug.Log("LOADER -- SoundManager.instance is NOT NULL. Existing instance will be used. ");
        }
    }

    
}
