﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Inspector Assigned Variables


    // Reference Cache
    public static GameManager instance = null;
    public GameObject scoreGO;

    // Private Variables
    private static int _currentScene = 0;

    private void Awake()
    {
        Debug.Log("GAMEMANAGER -- Awake() called");

        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            Debug.Log("GAMEMANAGER -- Awake() - instance != this - Destoying gameobject");
        }

        DontDestroyOnLoad(gameObject);

        
        InitGame();
    }

    
    // Use this for initialization
    private void Start()
    {
        Debug.Log("GAMEMANAGER -- Start() called");
        //Debug.Log("GAMEMANAGER -- Start() _scoreComponent is " + scoreGO.GetComponent<Score>() == null ? "NULL" : "FOUND");
       
    }
    
    private void InitGame()
    {
        if (_currentScene == 0)
        {
            //_scoreComponent = FindObjectOfType<Score>();
            scoreGO.GetComponent<Score>().AddPoints(876);
            Debug.Log("Add Points");
        }
    }

    private void OnEnable()
    {
        Debug.Log("GAMEMANAGER -- OnEnabled() called");
        
    }

    private void OnDisable()
    {
        Debug.Log("GAMEMANAGER -- OnDisabled() called");
    }

    private void OnDestroy()
    {
        Debug.Log("GAMEMANAGER -- OnDestroy() called");
    }


    public void SwitchScene()
    {
        if (_currentScene == 0)
        {
            scoreGO.GetComponent<Score>().AddPoints(876);
            Debug.Log("Add Points");
        }

        _currentScene = _currentScene == 0 ? 1 : 0;
        SceneManager.LoadScene(_currentScene);


    }

}
