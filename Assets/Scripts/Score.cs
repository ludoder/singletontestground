﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    // Inspector Assigned Variables
    public Text scoreText;

    // Reference Cache

    // Private Variables
    private int _score;

    private void Awake()
    {
        Debug.Log("SCORE -- Awake()");
        //scoreText = GetComponent<Text>();
        //Debug.Log(string.Format("SCORE -- scoreText Component is {0}", scoreText == null ? "NULL" : "FOUND"));
    }

    void Start()
    {
        _score = PlayerPrefs.GetInt("Score", 0);
        scoreText.text = "Score: " + _score;
    }

    private void OnGUI()
    {
        scoreText.text = "Score: " + _score;

    }

    public void AddPoints(int value)
    {
        _score += value;
        PlayerPrefs.SetInt("Score", _score);
        //Debug.Log(string.Format("SCORE -- AddPoints() - scoreText Component is {0}", scoreText == null ? "NULL" : "FOUND"));
        scoreText.text = "Score: " + _score;
    }

    private void OnDestroy()
    {
        Debug.Log("SCORE -- OnDestroy()");
    }
}
